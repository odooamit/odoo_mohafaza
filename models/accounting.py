# -*- coding: utf-8 -*-
from openerp import models, fields
import datetime
import logging

_logger = logging.getLogger(__name__)
YEARS = [(x, str(x)) for  x in range(2010, 2021)]
MONTHS = [(1, 'يناير'),
          (2, 'فبراير'),
          (3, 'مارس'),
          (4, 'إبريل'),
          (5, 'مايو'),
          (6, 'يونيو'),
          (7, 'يوليو'),
          (8, 'أغسطس'),
          (9, 'سبتمبر'),
          (10, 'أكتوبر'),
          (11, 'نوفمبر'),
          (12, 'ديسمبر')]


class Membership(models.Model):
    _name = 'mohafaza.account.memberships'
    member_id = fields.Many2one('mohafaza.members', string='إسم العضو', required=True, ondelete='restrict')
    date = fields.Date(string='التاريخ', default=fields.Date.today(), required=True)
    for_year = fields.Selection(string='عن عام', selection=YEARS)
    amount = fields.Float('المبلغ')
    notes = fields.Text('ملاحظات')

    _sql_constraints = [
        ('unique_membership', 'unique(member_id, for_year)', 'تم التسجيل من قبل')]


class KindergartenFee(models.Model):
    _name = 'mohafaza.account.kindergarten.fees'
    child_id = fields.Many2one('mohafaza.children', string='إسم الطفل', required=True, ondelete='restrict')
    kindergarten = fields.Selection(string='الحضانة', selection=[('hafez', 'الحافظ'), ('anamoslim', 'أنا المسلم')])
    date = fields.Date(string='التاريخ', default=fields.Date.today(), required=True)
    for_month = fields.Selection(string='عن شهر', selection=MONTHS)
    amount = fields.Float('المبلغ')
    notes = fields.Text('ملاحظات')

    _sql_constraints = [
        ('unique_kindergarten_fees', 'unique(child_id, for_month)', 'تم التسجيل من قبل')]


class BussFees(models.Model):
    _name = 'mohafaza.account.bus.fees'
    child_id = fields.Many2one('mohafaza.children', string='إسم الطفل', required=True, ondelete='restrict')
    date = fields.Date(string='التاريخ', default=fields.Date.today(), required=True)
    for_month = fields.Selection(string='عن شهر', selection=MONTHS)
    amount = fields.Float('المبلغ')
    notes = fields.Text('ملاحظات')

    _sql_constraints = [
        ('unique_bus_fees', 'unique(child_id, for_month)', 'تم التسجيل من قبل')]


class Salary(models.Model):
    _name = 'mohafaza.account.salaries'
    staff_id = fields.Many2one('mohafaza.staff', string='إسم الموظف', required=True, ondelete='restrict')
    date = fields.Date(string='التاريخ', default=fields.Date.today(), required=True)
    for_month = fields.Selection(string='عن شهر', selection=MONTHS)
    amount = fields.Float('المبلغ')
    notes = fields.Text('ملاحظات')